const express = require('express');
const app = express();
const morgan = require('morgan');
var cors = require('cors');

const routeLancamentos = require('./routes/lancamentos-routes.js');
const routeQrcodes = require('./routes/qrcodes-routes.js');
const routeUsuarios = require('./routes/usuarios-routes');
const routeEmpresas = require('./routes/empresas-routes');

app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(cors());

app.use('/lancamentos', routeLancamentos);

app.use('/qrcodes', routeQrcodes);

app.use('/usuarios', routeUsuarios);

app.use('/empresas', routeEmpresas);

app.use((req, res, next) => {
    const erro = new Error('Não encontrado');
    erro.status = 404;
    next(erro);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        mensagem: error.message
    });
});

module.exports = app;
