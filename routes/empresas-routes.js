const express = require('express');
const router = express.Router();
const empresaController = require('../controllers/empresas-controller');
const login = require('../middleware/login-middleware');

router.post('/', login.required, empresaController.insereEmpresas);
router.get('/', login.required, empresaController.getEmpresas);

module.exports = router;