const express = require('express');
const router = express.Router();
const lancamentosController = require('../controllers/lancamentos-controller');
const login = require('../middleware/login-middleware');

router.post('/', login.required, lancamentosController.insereLancamento);
router.get('/', login.required, lancamentosController.getLancamentos);
router.get('/usuario/:id_usuario', login.required, lancamentosController.getLancamentosUsuario);
router.get('/:id_lancamento', login.required, lancamentosController.getLancamento);
router.patch('/:id_lancamento', login.required, lancamentosController.alteraLancamento);


module.exports = router;