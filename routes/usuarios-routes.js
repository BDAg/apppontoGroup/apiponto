const express = require('express');
const router = express.Router();
const usuarioController = require('../controllers/usuario-controller');
const login = require('../middleware/login-middleware');

router.post('/', usuarioController.insereUsuario);
router.post('/login', usuarioController.login);
router.get('/', usuarioController.getUsuarios);

module.exports = router;