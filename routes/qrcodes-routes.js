const express = require('express');
const router = express.Router();
const qrcodeController = require('../controllers/qrcodes-controller');
const login = require('../middleware/login-middleware');

router.post('/', login.required, qrcodeController.insereQrcode);
router.get('/', login.required, qrcodeController.getQrcodes);

module.exports = router;