const mysql = require('../mysql');

exports.getEmpresas = (req, res, next) =>{
    mysql.getConnection((error, conn) =>{
        conn.query(
            'select * from empresas',(error, result, fields) =>{
                conn.release();
                if(error) {res.status(500).send({ error: error})}
                else{res.status(200).send({message: "Sucesso", empresas: result})}
            }
        );
    });
}

exports.insereEmpresas = (req, res, next) =>{
    mysql.getConnection((error, conn) => {
        conn.query(
            'insert into empresas (nome, latitude, longitude) values(?,?,?)',
            [req.body.nome, req.body.latitude, req.body.longitude],
            (error, result, fields) => {
                conn.release();

                if(error){res.status(500).send({ error : error })}
                else {res.status(201).send({ message : "Sucesso", id_usuario : result.insertId })}
            }
        );
    });
}