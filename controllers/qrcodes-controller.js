const mysql = require('../mysql');

exports.getQrcodes = (req, res, next) =>{
    mysql.getConnection((error, conn) =>{
        conn.query(
            'select * from qr_codes',(error, result, fields) =>{
                conn.release();
                if(error) {res.status(500).send({ error: error})}
                else{res.status(200).send({message: "Sucesso", qrcodes: result})}
            }
        );
    });
}

exports.insereQrcode = (req, res, next) =>{
    mysql.getConnection((error, conn) => {
        conn.query(
            'insert into qr_codes (qrcode, latitude, longitude, id_empresa) values(?,?,?,?)',
            [req.body.qrcode, req.body.latitude, req.body.longitude, req.body.id_empresa],
            (error, result, fields) => {
                conn.release();

                if(error){res.status(500).send({ error : error })}
                else {res.status(201).send({ message : "Sucesso", id_qrcode : result.insertId })}
            }
        );
    });
}