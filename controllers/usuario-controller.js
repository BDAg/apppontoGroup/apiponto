const mysql = require('../mysql');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.insereUsuario = (req, res, next)=>{
    mysql.getConnection((err, conn) => {
        if (err) {
            return res.status(500).send({error: err});
        } else{
            conn.query('select 1 from usuarios where usuario = ?', [req.body.usuario], (err, results) => {
                if(err) {
                    return res.status(500).send({error: err});
                } else if(results.length > 0) {
                    res.status(500).send({error: "Usuario já cadastrado"});
                } else {
                    bcrypt.hash(req.body.senha, 10, (errBcrypt, hash) => {
                        if(err){
                            res.status(500).send({ error: errBcrypt });
                        } else{
                            conn.query(
                                'insert into usuarios (nome, usuario, senha) values(?,?,?)',
                                [req.body.nome, req.body.usuario, hash],
                                (error, result, fields) => {
                                    conn.release();
                                    if(error){
                                        res.status(500).send({
                                            error : error
                                        });
                                    } else {
                                        let token = jwt.sign({
                                            nome: req.body.nome,
                                            usuario: req.body.usuario,
                                            id_usuario: req.body.id_usuario 
                                        }, "querino", {});
                                        res.status(201).send({
                                            message : "Sucesso",
                                            id_usuario : result.insertId,
                                            token: token
                                        });
                                    }
                                }
                            );
                        }
                    });
                }
            });
        }
    });
}

exports.login = (req, res, next) =>{
    mysql.getConnection((err, conn) => {
        if (err) { return res.status(500).send({error: err})}
        conn.query(`select * from usuarios where usuario = ?`, [req.body.usuario], (error, results) =>{
            conn.release();
            if (error) { return res.status(500).send({error: error})}
            if (results.length < 1) {return res.status(401).send({error: "Usuario não existe"})}
            else {
                bcrypt.compare(req.body.senha, results[0].senha, (errBcrypt, result) => {
                    if (errBcrypt) { return res.status(401).send({error: "Erro senha"})}
                    if (result) {
                        let token = jwt.sign({
                            nome: results[0].nome,
                            usuario: results[0].usuario,
                            id_usuario: results[0].id_usuario
                        }, "querino", {});
                        return res.status(200).send({
                            messagem: "Sucesso",
                            nome: results[0].nome,
                            usuario: results[0].usuario,
                            id_usuario: results[0].id_usuario,
                            token: token
                        })
                    } else {
                        return res.status(401).send({ error: "Falha na autenticação"})
                    }
                });
            }
        });
    });
}
exports.getUsuarios = (req, res, next) =>{
    mysql.getConnection((error, conn) =>{
        conn.query(
            'select * from usuarios',(error, result, fields) => {
                conn.release();
                if(error) {res.status(500).send({error: error})}
                else{res.status(200).send({message: "Sucesso", usuarios: result})}
            }
        );
    });
}