const mysql = require('../mysql');

exports.getLancamentos = (req, res, next) =>{
    mysql.getConnection((error, conn) =>{
        conn.query(
            'select * from lancamentos',(error, result, fields) => {
                conn.release();
                if(error) {res.status(500).send({error: error})}
                else{res.status(200).send({message: "Sucesso", lancamentos: result})}
            }
        );
    });
}

exports.getLancamentosUsuario = (req, res, next) =>{
    mysql.getConnection((error, conn) =>{
        conn.query(
            `select * from lancamentos where id_usuario = ?`,
            [req.params.id_usuario],
            (error, result, fields) => {
                conn.release();
                if(error) {res.status(500).send({error: error})}
                else{res.status(200).send({message: "Sucesso", lancamentos: result})}
            }
        );
    });
}

exports.getLancamento = (req, res, next) =>{
    mysql.getConnection((error, conn) =>{
        conn.query(`select * from lancamentos where id_lancamento= ?`, [req.params.id_lancamento], (error, result, fields) =>{
                conn.release();
                if(error) {res.status(500).send({ error: error})}
                else{res.status(200).send({message: "Sucesso", lancamento: result})}
            });
    });
}

exports.insereLancamento = (req, res, next) =>{
    mysql.getConnection((error, conn) => {
        conn.query(
            `insert into lancamentos (entrada, saida, id_qr_code, id_usuario) values(?,?,?,?)`,
            [req.body.entrada, req.body.saida, req.body.id_qr_code, req.body.id_usuario],
            (error, result, fields) => {
                conn.release();
                if(error){res.status(500).send({ error : error })}
                else {res.status(201).send({ message : "Sucesso", id_lancamento : result.insertId })}
            }
        );
    });
}

exports.alteraLancamento = (req, res, next) =>{
    mysql.getConnection((error, conn) =>{
        conn.query(
            `update lancamentos set saida= ? where id_lancamento = ?`,
            [req.body.saida, req.params.id_lancamento],
            (error, results, fiels) =>{
                conn.release();
                if(error){res.status(500).send({error : error})}
                else{res.status(201).send({message: "Sucesso"})}
            }
            );
    });
}
